package de.philippjanzen.masterthesis.warenkorbservice.communication;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import de.philippjanzen.masterthesis.warenkorbservice.produkt.Produkt;

@Service
public class RestApi implements ProduktAPI {

  RestTemplate restTemplate = new RestTemplate();
  String baseUrlProduktservice = "http://localhost:8010";

  public RestApi() {
  }

  public List<Produkt> leseProdukte() {

    ResponseEntity<List<Produkt>> response = restTemplate.exchange(baseUrlProduktservice + "/produkte",
        HttpMethod.GET, null, new ParameterizedTypeReference<List<Produkt>>() {
    });
    return response.getBody();
  }
}
