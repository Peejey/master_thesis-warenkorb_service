package de.philippjanzen.masterthesis.warenkorbservice.communication;

import java.util.List;

import de.philippjanzen.masterthesis.warenkorbservice.produkt.Produkt;

public interface ProduktAPI {

	public List<Produkt> leseProdukte();

}
