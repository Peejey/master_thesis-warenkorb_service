package de.philippjanzen.masterthesis.warenkorbservice.produkt;

import javax.persistence.Entity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Produkt extends AbstractProdukt {

	public Produkt() {

	}

}