package de.philippjanzen.masterthesis.warenkorbservice.produkt;

import org.springframework.data.repository.CrudRepository;

public interface ProduktRepository extends CrudRepository<Produkt, String> {

}
