package de.philippjanzen.masterthesis.warenkorbservice.produkt;

import org.springframework.beans.factory.annotation.Autowired;

import de.philippjanzen.masterthesis.warenkorbservice.mapper.WarenMapper;
import de.philippjanzen.masterthesis.warenkorbservice.warenkorb.Ware;
import de.philippjanzen.masterthesis.warenkorbservice.warenkorb.WarenInformationen;

public class ProduktInformationAdapter implements WarenInformationen {

	@Autowired
	private ProduktService produktService;

	public ProduktInformationAdapter() {
	}

	@Override
	public Ware leseInformationen(String wareId) {
		Produkt produkt = produktService.leseProdukt(wareId);
		return WarenMapper.mappeProduktZuWare(produkt);
	}

}
