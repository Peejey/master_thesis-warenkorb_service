package de.philippjanzen.masterthesis.warenkorbservice.produkt;

import java.util.UUID;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Size;

@MappedSuperclass
public abstract class AbstractProdukt {

	@Id
	private String id;
	private String name;
	@Size(max = 1020)
	private String beschreibung;
	private int lagerbestand;
	private double preis;

	protected AbstractProdukt() {

	}

	public AbstractProdukt(String name, String beschreibung, int lagerbestand, double preis) {
		this.name = name;
		this.beschreibung = beschreibung;
		this.lagerbestand = lagerbestand;
		this.preis = preis;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public int getLagerbestand() {
		return lagerbestand;
	}

	public void setLagerbestand(int lagerbestand) {
		this.lagerbestand = lagerbestand;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public void generateId() {
		setId(UUID.randomUUID().toString());
	}

}
