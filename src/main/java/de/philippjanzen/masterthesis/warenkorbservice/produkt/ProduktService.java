package de.philippjanzen.masterthesis.warenkorbservice.produkt;

import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.stereotype.Service;

import de.philippjanzen.masterthesis.warenkorbservice.communication.ProduktAPI;

@Service
public class ProduktService {

	private ProduktRepository produktRepository;
	private ProduktAPI produktApi;

	public ProduktService(ProduktRepository produktRepository, ProduktAPI produktApi) {
		this.produktRepository = produktRepository;
		this.produktApi = produktApi;
	}

	public Produkt leseProdukt(String id) {
		Produkt produkt;
		try {
			produkt = produktRepository.findById(id).get();
		} catch (NoSuchElementException e) {
			List<Produkt> produkte = produktApi.leseProdukte();
			produktRepository.saveAll(produkte);
			produkt = produkte.stream().filter(p -> p.getId().equals(id)).findFirst().get();
		}

		return produkt;
	}

}
