package de.philippjanzen.masterthesis.warenkorbservice.dtos;

public class WareAnfrageDTO {
	private String id;
	private int menge;

	public WareAnfrageDTO() {
	}

	public WareAnfrageDTO(String id, int menge) {
		super();
		this.id = id;
		this.menge = menge;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getMenge() {
		return menge;
	}

	public void setMenge(int menge) {
		this.menge = menge;
	}

}
