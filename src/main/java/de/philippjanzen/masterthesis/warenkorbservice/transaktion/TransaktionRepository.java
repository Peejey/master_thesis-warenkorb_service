package de.philippjanzen.masterthesis.warenkorbservice.transaktion;

import org.springframework.data.repository.CrudRepository;

public interface TransaktionRepository extends CrudRepository<Transaktion, String> {

}
