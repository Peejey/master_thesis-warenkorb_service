package de.philippjanzen.masterthesis.warenkorbservice.transaktion;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import de.philippjanzen.masterthesis.warenkorbservice.warenkorb.Ware;

@Service
public class TransaktionService {

	private TransaktionRepository transaktionRepository;

	public TransaktionService(TransaktionRepository transaktionRepository) {
		this.transaktionRepository = transaktionRepository;
	}

	@Transactional
	public Transaktion fuegeTransaktionHinzu(String id, List<Ware> waren) {
		Transaktion transaktion = new Transaktion(id, waren);
		return transaktionRepository.save(transaktion);
	}

	@Transactional
	public Transaktion leseTransaktion(String id) {
		return transaktionRepository.findById(id).get();
	}

}
