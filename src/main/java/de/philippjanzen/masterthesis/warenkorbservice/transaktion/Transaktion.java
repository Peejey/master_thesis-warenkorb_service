package de.philippjanzen.masterthesis.warenkorbservice.transaktion;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import de.philippjanzen.masterthesis.warenkorbservice.warenkorb.Ware;

@Entity
public class Transaktion {

	@Id
	private String id;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "transaktion_id")
	private List<Ware> waren = new ArrayList<>();

	public Transaktion() {
	}

	public Transaktion(String id, List<Ware> waren) {
		this.id = id;
		this.waren = waren;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Ware> getWaren() {
		return waren;
	}

	public void setWaren(List<Ware> waren) {
		this.waren = waren;
	}
}
