package de.philippjanzen.masterthesis.warenkorbservice.warenkorb;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;

import de.philippjanzen.masterthesis.sharedservice.events.DomainAggregate;
import de.philippjanzen.masterthesis.warenkorbservice.events.domainevents.WarenHinzugefuegtEvent;
import de.philippjanzen.masterthesis.warenkorbservice.events.domainevents.WarenkorbEvent;
import de.philippjanzen.masterthesis.warenkorbservice.events.domainevents.WarenkorbGeleertEvent;
import de.philippjanzen.masterthesis.warenkorbservice.utils.CalcUtils;

@Entity
public class Warenkorb extends DomainAggregate {

	@Id
	private String id;
	private int warenAnzahl;
	private double gesamtPreis;
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "warenkorb_id")
	private List<Ware> waren = new ArrayList<>();

	public Warenkorb() {

	}

	public Warenkorb(int warenAnzahl, double gesamtPreis, List<Ware> waren) {
		this.warenAnzahl = warenAnzahl;
		this.gesamtPreis = gesamtPreis;
		this.waren = waren;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getWarenAnzahl() {
		return warenAnzahl;
	}

	public void setWarenAnzahl(int warenAnzahl) {
		this.warenAnzahl = warenAnzahl;
	}

	public double getGesamtPreis() {
		return gesamtPreis;
	}

	public void setGesamtPreis(double gesamtPreis) {
		this.gesamtPreis = gesamtPreis;
	}

	public List<Ware> getWaren() {
		return waren;
	}

	public void setWaren(List<Ware> waren) {
		this.waren = waren;
	}

	public void fuegeWareHinzu(Ware ware) {
		waren.stream().filter(w -> w.getId().equals(ware.getId())).findFirst().map(w -> {
			w.setMenge(w.getMenge() + ware.getMenge());
			return w;
		}).orElseGet(() -> {
			waren.add(ware);
			return ware;
		});

		warenAnzahl += ware.getMenge();
		gesamtPreis = CalcUtils.round(gesamtPreis + ware.getGesamtPreis());
	}

	public WarenkorbEvent fuegeWarenHinzu(List<Ware> waren) {
		waren.forEach(ware -> {
			fuegeWareHinzu(ware);
		});
		return new WarenHinzugefuegtEvent();
	}

	public boolean loescheWare(String id) {
		Ware ware = waren.stream().filter(w -> w.getId().equals(id)).findFirst().get();
		if (waren.remove(ware)) {
			warenAnzahl -= ware.getMenge();
			gesamtPreis = CalcUtils.round(gesamtPreis - ware.getGesamtPreis());
			return true;
		}

		return false;

	}

	public void aendereWarenMenge(String wareId, int menge) {
		Ware ware = waren.stream().filter(w -> w.getId().equals(wareId)).findFirst().get();
		int alteMenge = ware.getMenge();
		double alterGesamtPreis = ware.getGesamtPreis();
		ware.setMenge(menge);

		warenAnzahl += (menge - alteMenge);
		gesamtPreis = CalcUtils.round(gesamtPreis + (ware.getGesamtPreis() - alterGesamtPreis));
	}

	@PrePersist
	private void generateId() {
		if (id == null || id.isEmpty()) {
			setId(UUID.randomUUID().toString());
		}
	}

	List<Ware> leeren() {
		List<Ware> tmpWaren = new ArrayList<Ware>(waren);
		waren = new ArrayList<>();
		warenAnzahl = 0;
		gesamtPreis = 0;
		return tmpWaren;
	}

	WarenkorbEvent leerenWithEvents() {
		waren = new ArrayList<>();
		warenAnzahl = 0;
		gesamtPreis = 0;
		return new WarenkorbGeleertEvent();
	}

}