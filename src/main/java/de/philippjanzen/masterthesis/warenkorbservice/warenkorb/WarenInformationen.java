package de.philippjanzen.masterthesis.warenkorbservice.warenkorb;

public interface WarenInformationen {

	public Ware leseInformationen(String wareId);
}
