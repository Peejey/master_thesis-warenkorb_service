package de.philippjanzen.masterthesis.warenkorbservice.warenkorb;

import org.springframework.data.repository.CrudRepository;

public interface WarenkorbRepository extends CrudRepository<Warenkorb, String> {

}
