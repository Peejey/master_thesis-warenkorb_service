package de.philippjanzen.masterthesis.warenkorbservice.warenkorb;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import de.philippjanzen.masterthesis.warenkorbservice.utils.CalcUtils;

@Entity
public class Ware {

	@Id
	private String id;
	private String name;
	private int menge;
	private double preis;
	private double gesamtPreis;

	public Ware() {
	}

	public Ware(String id, String name, int menge, double preis, double gesamtpreis) {
		this.id = id;
		this.name = name;
		this.menge = menge;
		this.preis = preis;
		this.gesamtPreis = gesamtpreis;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMenge() {
		return menge;
	}

	public void setMenge(int menge) {
		this.menge = menge;
		gesamtPreis = CalcUtils.round(menge * preis);
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(float preis) {
		this.preis = preis;
		gesamtPreis = CalcUtils.round(menge * preis);
	}

	public double getGesamtPreis() {
		return gesamtPreis;
	}

	public void setGesamtPreis(float gesamtpreis) {
		this.gesamtPreis = gesamtpreis;
	}
}
