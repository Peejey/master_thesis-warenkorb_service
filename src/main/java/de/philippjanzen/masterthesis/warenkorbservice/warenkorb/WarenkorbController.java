package de.philippjanzen.masterthesis.warenkorbservice.warenkorb;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import de.philippjanzen.masterthesis.warenkorbservice.dtos.WareAnfrageDTO;

@RestController
public class WarenkorbController {

	private WarenkorbService warenkorbService;

	public WarenkorbController(WarenkorbService warenkorbService) {
		this.warenkorbService = warenkorbService;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/warenkorb")
	public Warenkorb erstelleWarenkorb() {
		return warenkorbService.erstelleWarenkorb();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/warenkorb/{id}")
	public Warenkorb leseWarenkorb(@PathVariable String id) {
		return warenkorbService.leseWarenkorb(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/warenkorb/{warenkorbId}")
	public Warenkorb fuegeWareHinzu(@PathVariable String warenkorbId, @RequestBody WareAnfrageDTO wareAnfrage) {
		return warenkorbService.fuegeWareHinzu(warenkorbId, wareAnfrage.getId(), wareAnfrage.getMenge());
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/warenkorb/{warenkorbId}/{wareId}")
	public Warenkorb loescheWare(@PathVariable String warenkorbId, @PathVariable String wareId) {
		return warenkorbService.loescheWare(warenkorbId, wareId);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/warenkorb/{warenkorbId}")
	public void loescheAlleWaren(@PathVariable String warenkorbId) {
		warenkorbService.loescheAlleWaren(warenkorbId);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/warenkorb/{warenkorbId}/{wareId}")
	public Warenkorb aendereWarenMenge(@PathVariable String warenkorbId, @PathVariable String wareId,
			@RequestBody int menge) {
		return warenkorbService.aendereMenge(warenkorbId, wareId, menge);
	}
}