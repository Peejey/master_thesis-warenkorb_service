package de.philippjanzen.masterthesis.warenkorbservice.warenkorb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.philippjanzen.masterthesis.sharedservice.events.DomainEventPublisher;
import de.philippjanzen.masterthesis.warenkorbservice.events.domainevents.WarenkorbEvent;

@Service
public class WarenkorbService {

	private WarenkorbRepository warenkorbRepository;
	private WarenInformationen warenInformationen;

	@Autowired
	private DomainEventPublisher<WarenkorbEvent> domainEventPublisher;

	public WarenkorbService(WarenkorbRepository warenkorbRepository, WarenInformationen warenInformationen) {
		this.warenkorbRepository = warenkorbRepository;
		this.warenInformationen = warenInformationen;
	}

	public Warenkorb leseWarenkorb(String id) {
		return warenkorbRepository.findById(id).get();
	}

	@Transactional
	public Warenkorb fuegeWareHinzu(String warenkorbId, String wareId, int menge) {
		Ware ware = warenInformationen.leseInformationen(wareId);
		ware.setMenge(menge);
		Warenkorb warenkorb = warenkorbRepository.findById(warenkorbId).get();
		warenkorb.fuegeWareHinzu(ware);
		warenkorbRepository.save(warenkorb);
		return warenkorb;
	}

	@Transactional
	public Warenkorb fuegeWarenHinzu(String warenkorbId, List<Ware> waren) {
		Warenkorb warenkorb = warenkorbRepository.findById(warenkorbId).get();
		waren.forEach(ware -> {
			warenkorb.fuegeWareHinzu(ware);
		});
		warenkorbRepository.save(warenkorb);
		return warenkorb;
	}

	@Transactional
	public void fuegeWarenHinzuWithEvents(String warenkorbId, List<Ware> waren, String referenceId) {
		Warenkorb warenkorb = warenkorbRepository.findById(warenkorbId).get();
		WarenkorbEvent event = warenkorb.fuegeWarenHinzu(waren);
		warenkorbRepository.save(warenkorb);
		domainEventPublisher.publish(Warenkorb.class, warenkorb.getId(), referenceId, Collections.singletonList(event));
	}

	@Transactional
	public Warenkorb loescheWare(String warenkorbId, String wareId) {
		Warenkorb warenkorb = warenkorbRepository.findById(warenkorbId).get();
		if (!warenkorb.loescheWare(wareId)) {
			throw new IllegalArgumentException();
		}
		warenkorbRepository.save(warenkorb);
		return warenkorb;
	}

	@Transactional
	public Warenkorb aendereMenge(String warenkorbId, String wareId, int menge) {
		Warenkorb warenkorb = warenkorbRepository.findById(warenkorbId).get();
		warenkorb.aendereWarenMenge(wareId, menge);
		warenkorbRepository.save(warenkorb);
		return warenkorb;
	}

	@Transactional
	public Warenkorb erstelleWarenkorb() {
		Warenkorb warenkorb = new Warenkorb(0, 0, new ArrayList<Ware>());
		return warenkorbRepository.save(warenkorb);
	}

	@Transactional
	public List<Ware> loescheAlleWaren(String warenkorbId) {
		Warenkorb warenkorb = warenkorbRepository.findById(warenkorbId).get();
		List<Ware> waren = warenkorb.leeren();
		warenkorbRepository.save(warenkorb);
		return waren;
	}

	@Transactional
	public void loescheAlleWarenWithEvents(String warenkorbId, String referenceId) {
		Warenkorb warenkorb = warenkorbRepository.findById(warenkorbId).get();
		WarenkorbEvent event = warenkorb.leerenWithEvents();
		warenkorbRepository.save(warenkorb);
		domainEventPublisher.publish(Warenkorb.class, warenkorb.getId(), referenceId, Collections.singletonList(event));
	}

	@Transactional
	public void loescheWarenkorb(String warenkorbId) {
		warenkorbRepository.deleteById(warenkorbId);
	}

}
