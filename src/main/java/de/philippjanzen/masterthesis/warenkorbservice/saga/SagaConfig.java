package de.philippjanzen.masterthesis.warenkorbservice.saga;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.philippjanzen.masterthesis.sharedservice.messaging.management.ChannelManager;
import de.philippjanzen.masterthesis.sharedservice.messaging.rabbitmq.AmqpConnectionConfig;
import de.philippjanzen.masterthesis.sharedservice.saga.SagaCustomMessage;
import de.philippjanzen.masterthesis.sharedservice.saga.channels.ChannelMappingImpl;
import de.philippjanzen.masterthesis.sharedservice.saga.consumers.MessageConsumerImpl;
import de.philippjanzen.masterthesis.sharedservice.saga.producers.MessageProducerImpl;
import de.philippjanzen.masterthesis.warenkorbservice.saga.handlers.WarenkorbCommandHandlers;
import io.eventuate.tram.commands.common.ChannelMapping;
import io.eventuate.tram.messaging.common.Message;
import io.eventuate.tram.messaging.consumer.MessageConsumer;
import io.eventuate.tram.messaging.producer.MessageProducer;
import io.eventuate.tram.sagas.orchestration.SagaInstanceRepository;
import io.eventuate.tram.sagas.orchestration.SagaInstanceRepositoryJdbc;
import io.eventuate.tram.sagas.participant.SagaCommandDispatcher;
import io.eventuate.tram.sagas.participant.SagaLockManager;
import io.eventuate.tram.sagas.participant.SagaLockManagerImpl;

@Configuration
public class SagaConfig {

	@Bean
	SagaInstanceRepository sagaInstanceRepository() {
		return new SagaInstanceRepositoryJdbc();
	}

	@Bean
	MessageProducer messageProducer(RabbitTemplate rabbitTemplate, AmqpConnectionConfig rabbitConnectionConfig) {
		return new MessageProducerImpl(rabbitTemplate, rabbitConnectionConfig);
	}

	@Bean
	SagaLockManager sagaLockManager() {
		return new SagaLockManagerImpl();
	}

	@Bean
	ChannelMapping channelMapping() {
		return new ChannelMappingImpl();
	}

	@Bean
	MessageConsumer messageConsumer() {
		return new MessageConsumerImpl();
	}

	@Bean
	SagaCommandDispatcher bestellungCommandHandlers(WarenkorbCommandHandlers warenkorbCommandHandlers) {
		return new SagaCommandDispatcher("warenkorbService", warenkorbCommandHandlers.commandHandlers());
	}

	@Bean
	WarenkorbCommandHandlers produktCommandHandlers() {
		return new WarenkorbCommandHandlers();
	}
}
