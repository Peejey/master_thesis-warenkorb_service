package de.philippjanzen.masterthesis.warenkorbservice.saga.handlers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageHeaders;

import de.philippjanzen.masterthesis.sharedservice.dtos.LagerbestandsAktualisierungDTO;
import de.philippjanzen.masterthesis.sharedservice.saga.SagaConnectionConfig;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.AktualisiereLagerbestand;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.AktualisiereLagerbestandZurueck;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.LeereWarenkorb;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.LeereWarenkorbZurueck;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.SagaCustomCommandHandlersBuilder;
import de.philippjanzen.masterthesis.sharedservice.saga.producers.SagaCustomReplyMessageBuilder;
import de.philippjanzen.masterthesis.warenkorbservice.transaktion.TransaktionService;
import de.philippjanzen.masterthesis.warenkorbservice.warenkorb.Warenkorb;
import de.philippjanzen.masterthesis.warenkorbservice.warenkorb.WarenkorbService;
import io.eventuate.javaclient.spring.jdbc.IdGenerator;
import io.eventuate.tram.commands.common.CommandMessageHeaders;
import io.eventuate.tram.commands.consumer.CommandHandlers;
import io.eventuate.tram.commands.consumer.CommandMessage;
import io.eventuate.tram.messaging.common.Message;
import io.eventuate.tram.sagas.participant.SagaCommandHandlersBuilder;
import io.eventuate.tram.sagas.participant.SagaReplyMessageBuilder;

public class WarenkorbCommandHandlers {

	@Autowired
	private WarenkorbService warenkorbService;
	@Autowired
	private TransaktionService transaktionService;
	@Autowired
	private IdGenerator idGenerator;

	public WarenkorbCommandHandlers() {
	}

	public CommandHandlers commandHandlers() {
		return SagaCustomCommandHandlersBuilder.fromChannel(SagaConnectionConfig.WARENKORB_SERVICE_CHANNEL)
				.onMessage(LeereWarenkorb.class, this::leereWarenkorb)
				.onMessage(LeereWarenkorbZurueck.class, this::leereWarenkorbZurueck).build();
	}

	public Message leereWarenkorb(CommandMessage<LeereWarenkorb> cm) {
//		try {
//			Thread.sleep(20000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		LeereWarenkorb leereWarenkorb = cm.getCommand();
		String warenkorbId = leereWarenkorb.getWarenkorbId();
		transaktionService.fuegeTransaktionHinzu(leereWarenkorb.getTransactionId(),
				warenkorbService.loescheAlleWaren(warenkorbId));
		return new SagaCustomReplyMessageBuilder().withSuccess().withId(idGenerator.genId().asString())
				.withLock(Warenkorb.class, warenkorbId).build();
	}

	public Message leereWarenkorbZurueck(CommandMessage<LeereWarenkorbZurueck> cm) {
		LeereWarenkorbZurueck leereWarenkorbZurueck = cm.getCommand();
		String warenkorbId = leereWarenkorbZurueck.getWarenkorbId();
		warenkorbService.fuegeWarenHinzu(warenkorbId,
				transaktionService.leseTransaktion(leereWarenkorbZurueck.getTransactionId()).getWaren());
		return new SagaCustomReplyMessageBuilder().withSuccess().withId(idGenerator.genId().asString()).build();
	}
}
