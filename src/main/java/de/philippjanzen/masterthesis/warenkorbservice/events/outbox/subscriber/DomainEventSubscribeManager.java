package de.philippjanzen.masterthesis.warenkorbservice.events.outbox.subscriber;

import java.util.Collections;

import org.springframework.stereotype.Component;

import de.philippjanzen.masterthesis.sharedservice.events.DomainEventEnvelope;
import de.philippjanzen.masterthesis.sharedservice.events.EventsConnectionConfig;
import de.philippjanzen.masterthesis.sharedservice.messaging.management.ChannelManager;

@Component
public class DomainEventSubscribeManager {

	private ChannelManager<DomainEventEnvelope> channelManager;
	private BestellungEventSubscriber bestellungEventSubscriber;

	public DomainEventSubscribeManager(ChannelManager<DomainEventEnvelope> channelManager,
			BestellungEventSubscriber bestellungEventSubscriber) {
		this.channelManager = channelManager;
		this.bestellungEventSubscriber = bestellungEventSubscriber;
		subscribeToAllChannels();
	}

	private void subscribeToAllChannels() {
		channelManager.subscribe(bestellungEventSubscriber,
				Collections.singleton(EventsConnectionConfig.BESTELLUNG_SERVICE_CHANNEL));
	}

}
