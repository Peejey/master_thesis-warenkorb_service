package de.philippjanzen.masterthesis.warenkorbservice.events.domainevents.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.philippjanzen.masterthesis.warenkorbservice.events.domainevents.BestellungAbgebrochenEvent;
import de.philippjanzen.masterthesis.warenkorbservice.events.domainevents.BestellungAufgegebenEvent;
import de.philippjanzen.masterthesis.warenkorbservice.transaktion.TransaktionService;
import de.philippjanzen.masterthesis.warenkorbservice.warenkorb.WarenkorbService;

@Component
public class BestellungEventConsumer {

	@Autowired
	WarenkorbService warenkorbService;

	@Autowired
	TransaktionService transaktionService;

	public BestellungEventConsumer() {
	}

	public void accept(BestellungAufgegebenEvent bestellungAufgegebenEvent, String aggregateId, String correlationId) {
		transaktionService.fuegeTransaktionHinzu(aggregateId,
				warenkorbService.leseWarenkorb(bestellungAufgegebenEvent.getWarenkorbId()).getWaren());
		warenkorbService.loescheAlleWarenWithEvents(bestellungAufgegebenEvent.getWarenkorbId(), aggregateId);
	}

	public void accept(BestellungAbgebrochenEvent bestellungAbgebrochenEvent, String aggregateId,
			String correlationId) {
		warenkorbService.fuegeWarenHinzuWithEvents(bestellungAbgebrochenEvent.getWarenkorbId(),
				transaktionService.leseTransaktion(aggregateId).getWaren(), correlationId);
	}

}
