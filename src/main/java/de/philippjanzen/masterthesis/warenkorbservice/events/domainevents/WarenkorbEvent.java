package de.philippjanzen.masterthesis.warenkorbservice.events.domainevents;

import de.philippjanzen.masterthesis.sharedservice.events.DomainEvent;
import de.philippjanzen.masterthesis.sharedservice.events.EventsConnectionConfig;

public class WarenkorbEvent extends DomainEvent {

	@Override
	public String getChannel() {
		return EventsConnectionConfig.WARENKORB_SERVICE_CHANNEL;
	}

}
