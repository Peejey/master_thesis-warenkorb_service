package de.philippjanzen.masterthesis.warenkorbservice.events.domainevents;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BestellungAufgegebenEvent {

	@JsonProperty
	private String bestellDatum;
	@JsonProperty
	private String warenkorbId;

	protected BestellungAufgegebenEvent() {
	}

	public String getBestellDatum() {
		return bestellDatum;
	}

	public String getWarenkorbId() {
		return warenkorbId;
	}

}
