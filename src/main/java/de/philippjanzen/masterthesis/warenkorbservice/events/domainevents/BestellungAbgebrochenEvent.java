package de.philippjanzen.masterthesis.warenkorbservice.events.domainevents;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BestellungAbgebrochenEvent {

	@JsonProperty
	private String warenkorbId;

	protected BestellungAbgebrochenEvent() {
	}

	public String getWarenkorbId() {
		return warenkorbId;
	}

}
