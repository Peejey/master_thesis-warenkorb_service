package de.philippjanzen.masterthesis.warenkorbservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableAutoConfiguration
@EnableJpaRepositories(basePackages = { "de.philippjanzen.masterthesis.warenkorbservice",
		"de.philippjanzen.masterthesis.sharedservice.events" })
@ComponentScan(basePackages = { "de.philippjanzen.masterthesis.warenkorbservice",
		"de.philippjanzen.masterthesis.sharedservice.events" })
@EntityScan(basePackages = { "de.philippjanzen.masterthesis.warenkorbservice",
		"de.philippjanzen.masterthesis.sharedservice.events" })
public class WarenkorbserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(WarenkorbserviceApplication.class, args);
	}

}
