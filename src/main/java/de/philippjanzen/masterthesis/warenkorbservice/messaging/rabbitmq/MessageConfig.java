package de.philippjanzen.masterthesis.warenkorbservice.messaging.rabbitmq;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Declarables;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.philippjanzen.masterthesis.sharedservice.events.DomainEventEnvelope;
import de.philippjanzen.masterthesis.sharedservice.events.EventsConnectionConfig;
import de.philippjanzen.masterthesis.sharedservice.messaging.management.ChannelManager;
import de.philippjanzen.masterthesis.sharedservice.messaging.rabbitmq.AmqpConnectionConfig;
import de.philippjanzen.masterthesis.sharedservice.messaging.rabbitmq.AmqpConnectionConfigImpl;
import de.philippjanzen.masterthesis.sharedservice.messaging.rabbitmq.Receiver;
import de.philippjanzen.masterthesis.sharedservice.messaging.rabbitmq.SagaReceiver;
import de.philippjanzen.masterthesis.sharedservice.saga.SagaConnectionConfig;
import de.philippjanzen.masterthesis.sharedservice.saga.SagaCustomMessage;

@Configuration
public class MessageConfig {

	@Bean
	AmqpConnectionConfig rabbitmqConnectionConfig() {
		return new AmqpConnectionConfigImpl();
	}

	@Bean
	ConnectionFactory connectionFactory() {
		CachingConnectionFactory connectionFactory = new CachingConnectionFactory(
				rabbitmqConnectionConfig().getHostname());
		connectionFactory.setUsername("guest");
		connectionFactory.setPassword("guest");
		return connectionFactory;
	}

	Queue warenkorbSagaQueue() {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("x-dead-letter-exchange", "microservices-saga-dead-letter-exchange");
		return new Queue(SagaConnectionConfig.WARENKORB_SERVICE_CHANNEL, false, false, false, args);
	}

	Queue warenkorbEventsQueue() {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("x-dead-letter-exchange", "microservices-events-dead-letter-exchange");
		return new Queue(EventsConnectionConfig.WARENKORB_SERVICE_CHANNEL, false, false, false, args);
	}

	@Bean
	Declarables queues() {
		return new Declarables(warenkorbSagaQueue(), warenkorbEventsQueue());
	}

	TopicExchange sagaExchange() {
		return new TopicExchange(rabbitmqConnectionConfig().getSagaExchange());
	}

	TopicExchange eventsExchange() {
		return new TopicExchange(rabbitmqConnectionConfig().getEventsExchange());
	}

	@Bean
	Declarables exchanges() {
		return new Declarables(sagaExchange(), eventsExchange());
	}

	@Bean
	Declarables binding(AmqpConnectionConfig amqpConnectionConfig) {
		return new Declarables(
				BindingBuilder.bind(warenkorbSagaQueue()).to(sagaExchange())
						.with(amqpConnectionConfig.getRoute(SagaConnectionConfig.WARENKORB_SERVICE_CHANNEL)),
				BindingBuilder.bind(warenkorbEventsQueue()).to(eventsExchange())
						.with(amqpConnectionConfig.getRoute(EventsConnectionConfig.BESTELLUNG_SERVICE_CHANNEL)));
	}

	@Bean
	SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
			MessageListenerAdapter listenerAdapter) {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
		container.setConnectionFactory(connectionFactory);
		container.setQueueNames(SagaConnectionConfig.WARENKORB_SERVICE_CHANNEL,
				EventsConnectionConfig.WARENKORB_SERVICE_CHANNEL);
		container.setMessageListener(listenerAdapter);
		container.setDefaultRequeueRejected(false);
		return container;
	}

	@Bean
	public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
		final var rabbitTemplate = new RabbitTemplate(connectionFactory);
		rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
		return rabbitTemplate;
	}

	@Bean
	public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
		return new Jackson2JsonMessageConverter();
	}

	@Bean
	MessageListenerAdapter listenerAdapter(Receiver receiver) {
		MessageListenerAdapter messageListenerAdapter = new MessageListenerAdapter(receiver, "receiveMessage");
		messageListenerAdapter.setMessageConverter(producerJackson2MessageConverter());
		return messageListenerAdapter;
	}

	@Bean
	Receiver receiver() {
		return new Receiver(sagaQueueManager(), eventsQueueManager());
	}

	@Bean
	ChannelManager<SagaCustomMessage> sagaQueueManager() {
		return new ChannelManager<>();
	}

	@Bean
	ChannelManager<DomainEventEnvelope> eventsQueueManager() {
		return new ChannelManager<>();
	}
}
