package de.philippjanzen.masterthesis.warenkorbservice.mapper;

import de.philippjanzen.masterthesis.warenkorbservice.produkt.Produkt;
import de.philippjanzen.masterthesis.warenkorbservice.warenkorb.Ware;

public class WarenMapper {

	public static Ware mappeProduktZuWare(Produkt produkt) {
		return new Ware(produkt.getId(), produkt.getName(), 1, produkt.getPreis(), produkt.getPreis());
	}
}
