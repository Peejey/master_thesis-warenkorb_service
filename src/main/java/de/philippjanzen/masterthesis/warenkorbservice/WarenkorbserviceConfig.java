package de.philippjanzen.masterthesis.warenkorbservice;

import org.h2.server.web.WebServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import de.philippjanzen.masterthesis.warenkorbservice.communication.ProduktAPI;
import de.philippjanzen.masterthesis.warenkorbservice.communication.RestApi;
import de.philippjanzen.masterthesis.warenkorbservice.produkt.ProduktInformationAdapter;
import de.philippjanzen.masterthesis.warenkorbservice.warenkorb.WarenInformationen;
import io.eventuate.javaclient.spring.jdbc.IdGenerator;
import io.eventuate.javaclient.spring.jdbc.IdGeneratorImpl;

@Configuration
public class WarenkorbserviceConfig {

	private RestApi restApi;

	public WarenkorbserviceConfig() {
		restApi = new RestApi();
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedOrigins("http://localhost:4200")
						.allowedMethods("PUT", "DELETE", "GET", "POST").allowCredentials(false).maxAge(3600);
			}
		};
	}

	@Bean
	ServletRegistrationBean h2servletRegistration() {
		ServletRegistrationBean registrationBean = new ServletRegistrationBean(new WebServlet());
		registrationBean.addUrlMappings("/console/*");
		return registrationBean;
	}

	@Bean
	ProduktAPI produktApi() {
		return restApi;
	}

	@Bean
	IdGenerator idGenerator() {
		return new IdGeneratorImpl();
	}

	@Bean
	WarenInformationen warenInformation() {
		return new ProduktInformationAdapter();
	}

}
